package com.api.gateway

import com.github.tomakehurst.wiremock.client.WireMock.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = ["gateway.cucumber.uri=http://localhost:\${wiremock.server.port}"]
)
class RouteLocatorTest : ApiGatewayTest() {
    @Test
    fun testMethodNotAllowedWhenRequestedUsingPostMethod() {
        webClient
                .post().uri("/training")
                .exchange()
                .expectStatus().value {
                    status -> assertThat(status.equals(HttpStatus.METHOD_NOT_ALLOWED))
                }
    }

    @Test
    fun testPageLoadedWhenRequestedUsingGetMethod() {
        val responseBody = "The cucumber training"

        stubFor(get(urlEqualTo("/training"))
                .willReturn(
                        aResponse()
                                .withBody(responseBody)
                )
        )

        webClient
                .get().uri("/training")
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .consumeWith { response ->
                    assertThat(response.getResponseBody())
                            .isEqualTo(responseBody.toByteArray())
                }
    }

    @Test
    fun testPageLoadedWhenRequestedUsingFilter() {
        val responseBody = "The cucumber training"

        stubFor(get(urlEqualTo("/training/canon-case-study.pdf"))
                .willReturn(
                        aResponse()
                                .withBody(responseBody)
                )
        )

        webClient
                .get().uri("/rewrite")
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .consumeWith { response ->
                    assertThat(response.getResponseBody())
                            .isEqualTo(responseBody.toByteArray())
                }
    }
}
