package com.api.gateway

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient

@RunWith(SpringRunner::class)
@AutoConfigureWireMock(port = 0)
abstract class ApiGatewayTest {
    @Autowired
    protected lateinit var webClient: WebTestClient
}
