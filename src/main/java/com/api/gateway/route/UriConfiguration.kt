package com.api.gateway.route

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@ConfigurationProperties(prefix = "gateway")
@PropertySource("classpath:gateway.properties")
class UriConfiguration {
    @Value("\${gateway.cucumber.uri}")
    lateinit var cucumberUri: String
}
