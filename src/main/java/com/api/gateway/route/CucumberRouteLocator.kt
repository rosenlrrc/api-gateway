package com.api.gateway.route

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(UriConfiguration::class)
class CucumberRouteLocator {
    @Bean
    fun cucumberRouting(
            builder: RouteLocatorBuilder,
            uriConfiguration: UriConfiguration
    ): RouteLocator {
        return builder.routes()
                .route { r ->
                    r.path("/training")
                            .uri(uriConfiguration.cucumberUri)
                            .id("path_predicate")
                }
                .route { r ->
                    r.path("/rewrite")
                            .filters{ f -> f.rewritePath(
                                    "/rewrite",
                                    "/training/canon-case-study.pdf"
                            )}
                            .uri(uriConfiguration.cucumberUri)
                            .id("rewrite_filter")

                }

                .build()
    }
}
